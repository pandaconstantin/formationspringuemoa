---
title: "Lab1 Prise en main de Spring Boot"
format: pdf
---

## QCM 

1. L'injection de dépendance ou IOC est _______ ?
  
  A. Design Pattern
  
  B. Framework
  
  C. Module Java
  
  D. Framework ORM
  

2. Les Beans définis dans le framework spring sont par défaut ______?

  A. Abstrait
  
  B. Singleton
  
  C. Final
  
  D. Initialisé

3. Lequel des éléments suivants n’est pas un module de Spring ?

  A. AOP
  
  B. O/R Integration
  
  C. Spring MVC
  
  D. HTML/JSP

4. Lequel des énoncés suivants est vrai ?

  A. ApplicationContext implémente le BeanFactory

  B. ApplicationContext hérite de BeanFactory
 
  C. BeanFactory hérite de ApplicationContext

  D. BeanFactory implémente ApplicationContext


5. Quelle classe représente le conteneur IoC? 
 
  A. ApplicationContext

  B. ServletContext

  C. RootContext

  D. WebApplicationContext
  
6. Quel composant fait partie du couche d’accès aux données dans le Framework Spring?

  A Aspects

  B. JMS

  C. Beans

  D. Core

  E. Web

  F. Servelet

  G. Expression language


## Lab 1 - Créer une application Spring Framework

Créer un projet Maven avec les informations ci-dessous:

-   **Project Name**: gestionpersonne
-   **Group ID** : bf.sonabel
-   **package** : bf.sonabel.gestionpersonne

Ajouter les dépendances suivantes :

``` xml
<dependency>
    <groupId>org.springframework.core</groupId>
    <artifactId>org.springframework.core</artifactId>
    <version>3.2.4.RELEASE</version>
</dependency>
```

``` xml
<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>com.springsource.org.apache.commons.logging</artifactId>
    <version>1.1.1</version>
</dependency>
```

``` xml
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-beans</artifactId>
    <version>6.1.7</version>
</dependency>
```

```xml
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-context</artifactId>
  <version>5.3.24</version>
</dependency>
```

Soit le diagramme de classe suivant :

![](diagrammeclassepersonne.png){fig-align="center"}

1.  Créer la cette classe dans le package `bf.sonabel.gestionpersonne.composants`

2.  Créer le fichier de configuration `gestionPersonneConfiguration.xml` dans le classpath du projet

3.  Configurer le composant du type de `Personne` dans le ficher `gestionPersonneConfiguration.xml`

4.  Configurer le composant du type `Personne` en utilisant la méthode de l'annotation sous Spring.

## Lab 2 - Lire le fichier de configuration

Soit le fichier de configuration `configurationPersonneIOC.xml` dans le classpath dont le contenu est le suivant :

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="
        http://www.springframework.org/schema/beans   
        http://www.springframework.org/schema/beans/spring-beans-3.0.xsd">
   
    <bean id="injectSimple" class="bf.sonabel.gestionpersonne.composants.Personne">
        <property name="nom">
            <value>Smith</value>
        </property>
        <property name="prenom">
            <value>John</value>
        </property>
        <property name="age">
            <value>35</value>
        </property>
    </bean>
</beans>
```

1.  Afficher les informations du composant `injectSimple` en utilisant l'interface `BeanFactory`

2.  Afficher les informations du composant `injectSimple` en utilisant l'implementation de ApplicationContext `ClassPathXmlApplicationContext`

3.  Afficher les informations du composant `injectSimple` en utilisant l'implementation de ApplicationContext `FileSystemXmlApplicationContext`

4.  Lire la configuration Java via `AnnotationConfigApplicationContext`

5.  Lire la configuration Java via `SpringApplication`

## Lab 3 - L'injection de dépendance

Soient les classes suivantes :

``` java

public class Circle {
    
    public void draw(){
        System.out.println("circle is drawn");
    }

}
```

``` java
public class ShapeDrawer {
    
    Circle circle;
    
    public ShapeDrawer(Circle circle){
        this.circle = circle;
    }
    
    public void draw(){
        circle.draw();
    }
    

}
```

``` java
public class Client {
    
    public static void main(String[] args) {
        ShapeDrawer shapeDrawer = new ShapeDrawer(new Circle());
        shapeDrawer.draw();
    }

}
```

1.  Quelle est l'erreur dans la classe `ShapeDrawer` ? Corriger-la

2.  Ajouter la gestion de type `Triangle`

## Lab4 - L'injection de dépendances

Nous allons utiliser trois classes et une interface comme beans pour illustrer les concepts de CDI et SDI. Il s’agit respectivement des classes *Vehicle, ToyotaEngine, Tyres* et de l’interface *IEngine*.

À partir de notre exemple, nous pouvons voir que la classe *Vehicle* dépend de l’implémentation du *IEngine*, qui est une interface. La classe *ToyotaEngine* implémente l'interface et sa référence est fournie dans le fichier de configuration du bean mappé sur l'une des propriétés de la classe de *Vehicle*.

Dans la classe *Vehicle*, nous invoquons le contexte d'application et l'instanciation du bean est exécutée. Deux objets de classe *Vehicle* sont instanciés. **'obj1'** est instancié via un bean portant le nom *InjectwithConstructor*. Le nom du bean peut se trouver dans le fichier de configuration du bean. De même, **'obj2'** est instancié via un bean portant le nom *InjectwithSetter*.

On peut observer que «obj1» est injecté via le constructeur et «obj2» utilise l'injection setter.

Dans le fichier de configuration du bean ci-dessous, nous avons utilisé deux déclarations de beans *Vehicle*.

Le bean *InjectwithConstructor* utilise l'élément constructor-arg, avec les attributs *name* et *ref*. L'attribut *'name'* est en corrélation avec le nom de l'argument du constructeur donné dans la définition de la classe *Vehicle*. Et l'attribut *'ref'* pointe vers la référence du bean qui peut être utilisée pour l'injection.

*InjectwithSetter* utilise l'élément de propriété pour fournir le «nom» de la propriété et la «valeur» de la propriété. À la place de la valeur, l'attribut «ref » peut être utilisé pour désigner une référence à un bean.

Dans les détails de configuration, nous injectons la référence *ToyotaBean* dans la référence *IEngine* via le constructor-arg de la classe *Vehicle*, où *IEngine* est une interface et nécessite une référence de classe d'implémentation pour l'injection de bean.

Nous allons utiliser deux références de beans distinctes pour la classe *Tyres*, à injecter respectivement via le setter et le constructeur. Nous pourront observer que 'tyre1Bean' et 'tyre2Bean' sont initialisés avec des valeurs littérales String pour chacune des propriétés.

![](maquetteexercice.png){fig-align="center" width="30%"}

## Lab5 Spring Boot - Spring Data JPA


Étant donné que nous disposons d'un système de notification qui doit envoyer à la fois des e-mails et des SMS aux clients, nous pouvons modéliser les relations de notification comme suit :



![](classeNotification.png){fig-align="center" width="50%"}

1. Créer une application Spring Boot avec les informations suivantes pour l'artefact : 

-   **Project Name**: gestionnotification
-   **Group ID** : com.exemple 
-   **package** : com.exemple.gestionnotification


2. Avec Spring Initializr , ajouter le starter suivant : **Spring Data JPA**


3. Créer deux bases de données PostgreSQL nommées **gestionnotificationdbDev** et **gestionnotificationdbProd**. Configurer votre application Spring Boot pour qu'elle puisse se connecter à la base *gestionnotificationdbDev* lorsqu'elle s'execute avec le profil de développement   et *gestionnotificationdbProd* lorsqu'elle s'exécute avec le profil production.


4. Créer les classes entités dont le diagramme est définit dans la figure ci-dessus.


5. Créer un composant de type Service qui crée dans la base de données de dev 3 notifications différrentes de chaque à 3 personnes differentes





